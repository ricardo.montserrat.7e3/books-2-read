package cat.itb.books2read.Database;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import cat.itb.books2read.Utilities.Status;

@Entity (tableName = "Books")
public class Book implements Parcelable
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_book")
    private int id;
    private String title, author;
    private Status status;
    private float rating;

    public Book()
    {
        title = "";
        author = "";
        status = Status.WantsToRead;
    }

    public Book(String title, String author, Status status)
    {
        this.title = title;
        this.author = author;
        this.status = status;
    }

    protected Book(Parcel in)
    {
        id = in.readInt();
        title = in.readString();
        author = in.readString();
        rating = in.readFloat();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>()
    {
        @Override
        public Book createFromParcel(Parcel in)
        {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size)
        {
            return new Book[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Status getStatus() { return status; }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setRate(float rating) { this.rating = rating; }

    public int getId() {
        return id;
    }

    public void setId(int id) { this.id = id; }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(status.name());
        dest.writeFloat(rating);
    }
}

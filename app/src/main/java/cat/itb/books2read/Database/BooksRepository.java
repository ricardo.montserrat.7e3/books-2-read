package cat.itb.books2read.Database;

import java.util.List;

public class BooksRepository
{
    private BookDAO bookDAO;

    public BooksRepository(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    public void insert(Book book){ bookDAO.insert(book);}

    public void update(Book book){ bookDAO.update(book);}

    public void delete(Book book){ bookDAO.delete(book);}

    public List<Book> getAll(){ return bookDAO.getAll();}
}

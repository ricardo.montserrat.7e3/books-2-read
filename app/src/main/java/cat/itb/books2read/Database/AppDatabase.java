package cat.itb.books2read.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import cat.itb.books2read.Utilities.Status;

@Database(entities = {Book.class}, version = 1, exportSchema = false)
@TypeConverters(StatusConverter.class)
public abstract class AppDatabase extends RoomDatabase
{
    public static AppDatabase instance;

    public abstract BookDAO booksDao();

    public static AppDatabase getInstance(Context context) { return instance != null? instance :
            Room.databaseBuilder(context, AppDatabase.class, "books2read.db").allowMainThreadQueries().fallbackToDestructiveMigration().build(); }
}

class StatusConverter
{

    @TypeConverter
    public static String fromStatusToInt(Status value) {
        return value.toString();
    }

    @TypeConverter
    public static Status fromStringToStatus(String value) {
        return Status.valueOf(value);
    }

}

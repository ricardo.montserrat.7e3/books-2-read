package cat.itb.books2read;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Bundle;

import cat.itb.books2read.Model.DataViewModel;

public class MainActivity extends AppCompatActivity
{
    public NavController navController;
    public static DataViewModel dataViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(dataViewModel == null)
        {
            dataViewModel = new ViewModelProvider(this).get(DataViewModel.class);
            dataViewModel.initialize(this);
        }

        NavHostFragment navHostFragment = (NavHostFragment)getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_container);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();
    }
}
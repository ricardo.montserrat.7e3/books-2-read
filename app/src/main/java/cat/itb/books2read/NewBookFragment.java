package cat.itb.books2read;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import cat.itb.books2read.Database.Book;
import cat.itb.books2read.Utilities.Status;

public class NewBookFragment extends Fragment implements AdapterView.OnItemSelectedListener
{
    private TextView rateTitle;
    private EditText titleEdit, authorEdit;
    private Spinner status;
    private RatingBar ratingBar;

    private Book currentBook;
    private boolean isEditing;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.new_book, container, false);

        titleEdit = v.findViewById(R.id.title_edit);
        authorEdit = v.findViewById(R.id.author_edit);
        status = v.findViewById(R.id.status_spinner);
        ratingBar = v.findViewById(R.id.rating_bar);
        rateTitle = v.findViewById(R.id.rate);

        Button addOrUpdateButton = v.findViewById(R.id.add_new_book_button);

        if(getArguments() != null)
        {
            currentBook = NewBookFragmentArgs.fromBundle(getArguments()).getBook();
            assert currentBook != null;
            titleEdit.setText(currentBook.getTitle());
            authorEdit.setText(currentBook.getAuthor());
            status.setSelection(currentBook.getStatus().ordinal());
            status.setOnItemSelectedListener(this);
            ratingBar.setRating(currentBook.getRating());

            isEditing = !titleEdit.getText().toString().isEmpty();
        }

        addOrUpdateButton.setText(isEditing? "Update Book Info" : "Add New Book");
        addOrUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!titleEdit.getText().toString().isEmpty() && !authorEdit.getText().toString().isEmpty())
                {
                    currentBook.setTitle(titleEdit.getText().toString());
                    currentBook.setAuthor(authorEdit.getText().toString());
                    currentBook.setStatus(Status.valueOf(status.getSelectedItem().toString().replaceAll("\\s","")));
                    if(currentBook.getStatus() == Status.Read) currentBook.setRate(ratingBar.getRating());

                    if(isEditing) MainActivity.dataViewModel.updateBook(currentBook);
                    else MainActivity.dataViewModel.addBook(currentBook);

                    Navigation.findNavController(v).navigate(R.id.action_newBookFragment_to_bookListFragment2);
                }
                else Toast.makeText(v.getContext(), "Please Assign all the values first.", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        ratingBar.setVisibility(status.getSelectedItem().equals(Status.Read.name())? View.VISIBLE : View.INVISIBLE);
        rateTitle.setVisibility(ratingBar.getVisibility());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { status.setSelection(0); }
}

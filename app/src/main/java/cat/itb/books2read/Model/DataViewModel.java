package cat.itb.books2read.Model;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import cat.itb.books2read.Database.AppDatabase;
import cat.itb.books2read.Database.Book;
import cat.itb.books2read.Database.BooksRepository;
import cat.itb.books2read.R;
import cat.itb.books2read.Utilities.BookListAdapter;
import cat.itb.books2read.Utilities.Status;

public class DataViewModel extends ViewModel
{
    private BooksRepository repoBooks;
    private BookListAdapter adapter;

    public DataViewModel() { }

    public void initialize(Context context)
    {
        AppDatabase tempData = AppDatabase.getInstance(context);
        repoBooks = new BooksRepository(tempData.booksDao());

        adapter = new BookListAdapter(R.layout.book_view_item, repoBooks.getAll());
        if(adapter.getItemCount() < 1) createBookExamples();
    }
    //TODO: Make cute layout

    public Book getBook(int position) { return adapter.getItem(position); }

    private void createBookExamples() {
        for (int i = 1; i <= 3; i++)
        {
            Book testBook = new Book("My favorite book " + i, "Books2Read", Status.Reading);
            testBook.setId(i);
            adapter.addBook(testBook);
        }
    }

    public void addBook(Book book)
    {
        adapter.addBook(book);
        repoBooks.insert(book);
    }

    public void removeBook(int position) { repoBooks.delete(adapter.removeItem(position)); }

    public void updateBook(Book book)
    {
        adapter.updateBook(book);
        repoBooks.update(book);
    }

    public BookListAdapter getAdapter() { return adapter; }
}

package cat.itb.books2read.Utilities;

public enum Status
{
    WantsToRead,
    Reading,
    Read
}

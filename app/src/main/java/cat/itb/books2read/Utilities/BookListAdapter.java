package cat.itb.books2read.Utilities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.books2read.BookListFragmentDirections;
import cat.itb.books2read.Database.Book;
import cat.itb.books2read.R;

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder>
{
    private int layout;
    private List<Book> bookList;

    public BookListAdapter(int layout, List<Book> bookList)
    {
        this.layout = layout;
        this.bookList = bookList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) { return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout, parent, false)); }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        final Book currentBook = bookList.get(position);
        holder.title.setText(currentBook.getTitle());
        holder.author.setText(currentBook.getAuthor());
        holder.status.setText(currentBook.getStatus().name());
        if(holder.status.getText().equals(Status.Read.name()))
        {
            holder.rating.setText(String.valueOf(currentBook.getRating()));
            holder.rating.setVisibility(View.VISIBLE);
        }
        else holder.rating.setVisibility(View.INVISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                NavDirections navDirections = BookListFragmentDirections.actionBookListFragmentToNewBookFragment(bookList.get(position));
                Navigation.findNavController(v).navigate(navDirections);
            }
        });
    }

    @Override
    public int getItemCount() { return bookList.size(); }

    public Book getItem(int position) { return bookList.get(position); }

    public Book removeItem(int position)
    {
        Book removedBook = bookList.remove(position);
        this.notifyDataSetChanged();
        return removedBook;
    }

    public void addBook(Book newBook) { bookList.add(newBook); this.notifyDataSetChanged(); }

    public void updateBook(Book book)
    {
        for (int i = 0; i < bookList.size(); i++) if(bookList.get(i).getId() == book.getId()) bookList.set(i, book);
        this.notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView title, author, status, rating;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            title = itemView.findViewById(R.id.text_view_title);
            author = itemView.findViewById(R.id.text_view_author);
            status = itemView.findViewById(R.id.text_view_status);
            rating = itemView.findViewById(R.id.text_view_rating);
        }
    }
}
